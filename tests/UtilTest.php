<?php

namespace App\Tests;

use App\Entity\Guardian;
use PHPUnit\Framework\TestCase;

class UtilTest extends TestCase
{
    public function testGuardian(): void
    {
        $test = new Guardian();
        $email = "test@test.fr";
        $test->setEmail("test@test.fr");

        $this->assertTrue($test->getEmail() === $email);
    }
}
