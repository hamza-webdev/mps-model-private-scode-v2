<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EleveTest extends WebTestCase
{
    public function testDisplayEleve(): void
    {
        $client = static::createClient();
        // $client->followRedirect();
        $crawler = $client->request('GET', '/eleve/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Eleve index');
    }
}
