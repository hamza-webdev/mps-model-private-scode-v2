<?php

namespace App\Test\Controller;

use App\Entity\Demo;
use App\Repository\DemoRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DemoControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private DemoRepository $repository;
    private string $path = 'https://localhost:8000/demo/';

/* @see \Doctrine\DBAL\Driver\Middleware */
    // protected function setUp(): void
    // {
    //     $this->client = static::createClient();
    //     $repository = static::getContainer()->get('doctrine')->getRepository(Demo::class);

    //     foreach ($this->repository->findAll() as $object) {
    //         $this->repository->remove($object, true);
    //     }
    // }

/* @see \Doctrine\DBAL\Driver\Middleware  */
    public function testIndex(): void
    {
        $client = static::createClient();
        $repository = static::getContainer()->get('doctrine')->getRepository(Demo::class);
        $crawler = $client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Demo index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $client = static::createClient();
         $repository = static::getContainer()->get('doctrine')->getRepository(Demo::class);

        $originalNumObjectsInRepository = count($repository->findAll());

        $this->markTestIncomplete();
        $client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $client->submitForm('Save', [
            'demo[name]' => 'Testing',
        ]);

        self::assertResponseRedirects('/demo/');

        self::assertSame($originalNumObjectsInRepository + 1, count($repository->findAll()));
    }

    public function testShow(): void
    {
        $client = static::createClient();
        $repository = static::getContainer()->get('doctrine')->getRepository(Demo::class);

        $this->markTestIncomplete();
        $fixture = new Demo();
        $fixture->setName('My Title');

        $repository->add($fixture, true);

        $client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Demo');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $client = static::createClient();
        $repository = static::getContainer()->get('doctrine')->getRepository(Demo::class);
        $this->markTestIncomplete();
        $fixture = new Demo();
        $fixture->setName('My Title');

        $repository->add($fixture, true);

        $client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $client->submitForm('Update', [
            'demo[name]' => 'Something New',
        ]);

        self::assertResponseRedirects('/demo/');

        $fixture = $repository->findAll();

        self::assertSame('Something New', $fixture[0]->getName());
    }

    public function testRemove(): void
    {
        $client = static::createClient();
        $repository = static::getContainer()->get('doctrine')->getRepository(Demo::class);
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($repository->findAll());

        $fixture = new Demo();
        $fixture->setName('My Title');

        $repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($repository->findAll()));

        $client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($repository->findAll()));
        self::assertResponseRedirects('/demo/');
    }

}
